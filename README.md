This project serves to demonstrate a bug in Gitlab's CI wherein artifacts are unbrowsable despite existing, the reason being that the files are empty. Suppose we have the repo

    .gitlab-ci.yml
    src/
      ...
    public
      index.html

Wherein index.html is blank as a result of broken source code. The page will display (as a blank document) when visited in the gitlab.io domain, but the artifact will not be entirely accessible.

When observing the pipeline through https://gitlab.com/Krakob/broken-artifact/-/jobs/XXXXXXX you will be able to download a .zip once the job finishes, but browsing will result in a 404 page. If you refresh the job page, the download/browse buttons will not appear.

The page correctly displays as a blank document at https://krakob.gitlab.io/broken-artifact/
